#!/bin/bash

# Optional in execution dir: buildVars.sh
# Mandatory in execution dir: build.properties, buildUtils.sh
# Script checks to see if these paths exist, and tries to pull the files if they don't exist: ${BUILDPATH}/${XNAT_REPO} & ${BUILDPATH}/${CUSTOM_REPO}

# Run XNAT setup and update process

# Set variables
echo Setting variable paths

# Access build utility functions
source ${CURDIR}/buildUtils.sh || die "Couldn't source ${CURDIR}/buildUtils.sh"

if [ -s ${CURDIR}/buildVars.sh ]; then
	source ${CURDIR}/buildVars.sh || die "${CURDIR}/buildVars.sh exists, but couldn't source the file"
fi	
	
# Cleaning out webapp directory
if [ -d ${TOMCAT_HOME}/webapps/${APP_NAME} ]; then
    rm -rf ${TOMCAT_HOME}/webapps/${APP_NAME}/* || die "Couldn't purge {TOMCAT_HOME}/webapps/${APP_NAME}"
fi 

# Cleaning out build directory
if [ -d ${BUILDPATH}/xnat ]; then
    rm -rf ${BUILDPATH}/xnat || die "Couldn't delete ${BUILDPATH}/xnat"
fi

# LINES TO BE EXECUTED IN NEW LOCAL ENVIRONMENT ONLY
# Create DB
if [ "$BUILD_TYPE" == "NEW" ]; then
	$POSTGRES_BIN/dropdb -U $DBUSER -h $DBHOST $DBNAME
	$POSTGRES_BIN/createdb -U $DBUSER -h $DBHOST $DBNAME
	$POSTGRES_BIN/createlang -U $DBUSER -h $DBHOST plpgsql $DBNAME
fi

# Check to make sure build.properties exists
if [ ! -s ${CURDIR}/build.properties ]; then
  echo Cannot find ${CURDIR}/build.properties
  exit 1;
fi

# If either expected source path  doesn't exist, then go fetch it
if [ ! -d ${BUILDPATH}/${XNAT_REPO} ] || ([ ! -z ${CUSTOM_REPO} ] && [ ! -d ${BUILDPATH}/${CUSTOM_REPO} ]); then
   ${CURDIR}/getBuildFiles.sh || die "${CURDIR}/getBuildFiles.sh failed"
fi

setupStr="bin/setup.sh"
updateStr="bin/update.sh -Ddeploy=true"
if [ ! -z ${RUN_MODULES_STATE} ]; then
    if [ "${RUN_MODULES_STATE}" = "SETUP" ]; then
        updateStr="${updateStr} -DskipModules=true"
    elif [ "${RUN_MODULES_STATE}" = "UPDATE" ]; then
        setupStr="${setupStr} -DskipModules=true"
    fi
fi

if [ -z ${CUSTOM_REPO} ] || [ ${BUILD_TYPE} = "NEW" ]; then
     setupStr="${setupStr} -Ddeploy=true"
fi

echo Preparing to run setup
mv ${BUILDPATH}/${XNAT_REPO} ${BUILDPATH}/xnat || die "Could not move ${XNAT_REPO} to ${BUILDPATH}/xnat" 
cp -f ${CURDIR}/build.properties ${BUILDPATH}/xnat || die "Could not copy build.properties into ${BUILDPATH}/xnat"
cd ${BUILDPATH}/xnat
${setupStr} | tee .setup.log 

FAIL=`cat .setup.log | grep -i "BUILD FAILED"`
rm .setup.log

if [ ! -z "${FAIL}" ]; then
    die "XNAT setup failed"
fi

if [ ${BUILD_TYPE} = "NEW" ]; then
    # Create the XNAT database
    cd ${BUILDPATH}/xnat/deployments/${APP_NAME}
    echo "Deploying setup sql"
    psql -f sql/${APP_NAME}.sql -h ${DBHOST} -U ${DBUSER} -d ${DBNAME} || die "Database setup failed"
fi

cd ${BUILDPATH}

# If a custom repo was specified, then copy it in
if [ ! -z ${CUSTOM_REPO} ]; then
    # Verify app directory exists in XNAT build directory
    echo Verify that ${CUSTOM_REPO} exists 
    if [ ! -d ${BUILDPATH}/xnat/projects/${APP_NAME} ]; then
        echo Could not find xnat/projects/${APP_NAME}
        exit 1;
    fi

    # Copy the custom directories into XNAT
    echo Pre copy
    find ${BUILDPATH}/${CUSTOM_REPO} -name '*' | xargs touch || die "touching project files failed"
    cp -rf ${BUILDPATH}/${CUSTOM_REPO}/* ${BUILDPATH}/xnat/projects/${APP_NAME} || die "copy of ${APP_NAME} dir failed"
    echo Post CNDA copy

    if [ ! -z ${CUSTOM_REPO2} ]; then
       # Copy the custom directories into XNAT
       echo Pre repo2 copy
       find ${BUILDPATH}/${CUSTOM_REPO2} -name '*' | xargs touch || die "touching project files failed"
       cp -rf ${BUILDPATH}/${CUSTOM_REPO2}/* ${BUILDPATH}/xnat/projects/${APP_NAME} || die "copy of ${CUSTOM_REPO2} dir failed"
       echo Post repo2 copy
    fi
fi

if [ ! -z ${CUSTOM_REPO} ] || [ ${BUILD_TYPE} = "EXISTING" ]; then
    # Run the update
    echo Preparing to run update
    cd ${BUILDPATH}/xnat
    ${updateStr} | tee .update.log
 
    FAIL=`cat .update.log | grep -i "BUILD FAILED"`
    rm .update.log

    if [ ! -z "${FAIL}" ]; then
        die "XNAT update failed"
    fi 

    # Update the database
    cd ${BUILDPATH}/xnat/deployments/${APP_NAME}
    echo "Deploying update sql"
    psql -f sql/${APP_NAME}-update.sql -h ${DBHOST} -U ${DBUSER} -d ${DBNAME} || die "Database update failed"
fi

if [ ${BUILD_PURPOSE} = "DEV" ]; then
    chmod -R 777 ${TOMCAT_HOME}/webapps/${APP_NAME}
fi

cd ${CURDIR}
