#!/bin/bash

SCRIPTNAME=nrg-vm-setup.sh
CUR_HOST=`hostname -s`

# Usage output for explaining how to use the program
function usage
{
	echo "${SCRIPTNAME} deploys NRG dev vm based on contents of the environment's build config repo."
	echo ""
	echo "Usage: ${SCRIPTNAME} (REQUIRED) -P project -R repo -O owner (OPTIONS) -H homedir -C changeset -D repodir -X xnatid"	
	echo "   REQUIRED"
        ### Features not yet working ######################
        #echo "-L || --language        Language (hg,git)"
        #echo "-S || --site            Site name (bitbucket.org,github.com)"
        ###################################################
        echo "-P || --project         NRG project name (cnda,dian,tip)"
	     echo "-R || --repo            Repository name"
 	     echo "-O || --owner           Repository owner"
	     echo ""
	     echo "   OPTIONAL" 
        echo "-H || --homedir         Home directory of Linux user running webapp" 
        echo "-C || --changeset       Repository changeset -- if not provided, then default is most recent commit."
        echo "-D || --repodir         Directory within repository containing desired build configuration."
        echo "-X || --xnatid          VM Owner's XNAT user id (if available)"
        echo ""
}

# If no arguments are passed, display usage screen
if [ $# == 0 ]
then
	usage
	exit 1
fi

# Scan through user provided arguments.
while [ "$1" != "" ]; do
	case $1 in 
                -H | --homedir )
                        shift
                        homedir=$1
                        ;;
                -P | --project )
                        shift
                        project=$1
                        ;;
                -L | --language )
                        shift
                        language=$1
                        ;;
                -S | --site )
                        shift
                        site=$1
                        ;;
              	 -R | --repo )
			               shift
                        repo=$1
			               ;;
		          -O | --owner )
			               shift
			               owner=$1
			               ;;
		          -C | --changeset )
			               shift
			               changeset=$1
			               ;;
                -X | --xnatid )
                        shift
                        xnatid=$1
                        ;;
                -D | --repodir )
                        shift
                        repodir=$1
                        ;;
		          -h | --help )
			              usage
			              exit 1
    esac
    shift
done

site=bitbucket.org
language=hg
### Features not yet working ######################
#if [ -z ${site} ]; then
#   echo -S or --site parameter is required.
#   exit 1
#fi

#if [ -z ${language} ]; then
#   echo -L or --language parameter is required.
#   exit 1
#fi
###################################################

if [ -z ${project} ]; then
   echo -P or --project parameter is required.
   exit 1
fi

if [ -z ${repo} ]; then
   echo -R or --repo parameter is required.
   exit 1
fi

if [ -z ${owner} ]; then
   echo -O or --owner parameter is required.
   exit 1
fi

export CUR_HOST=`hostname -s`
echo current host: $CUR_HOST

if [ -z ${homedir} ]; then
    homedir=~
fi

# Make clean copy of build directory
if [ -d ~/${project,,}_build/${CUR_HOST}/build_scripts ]; then
    rm -rf ~/${project,,}_build/${CUR_HOST}/build_scripts
fi

# Make clean copy of build config directory
if [ -d ~/${project,,}_build/${CUR_HOST}/${repo} ]; then
    rm -rf ~/${project,,}_build/${CUR_HOST}/${repo}
fi

# If build directory doesn't exist, create it
if [ ! -d ~/${project,,}_build/${CUR_HOST} ]; then
    mkdir --parents ~/${project,,}_build/${CUR_HOST}
fi
cd ${homedir}/${project,,}_build/${CUR_HOST}

if [ "${site}" = "bitbucket.org" ] &&  [ "${language}" = "hg" ]; then

    if [ -z ${changeset} ]; then
        changeset=tip
        echo "Using "tip" as changeset version."
    fi

    ${language} clone -r ${changeset} ssh://hg@${site}/${owner}/${repo}

    cp -f ${repo}/* .
    if [ ! -z ${repodir} ] && [ -d ${repo}/${repodir} ]; then
        cp -rf ${repo}/${repodir}/* .
    fi

    # Source the buildVar.sh
    if [ ! -s ./buildVars.sh ]; then
        echo No buildVars.sh found in ${repo}.  buildVars.sh is required to run NRG vm setup.
        exit 1
    else
        source ./buildVars.sh
    fi

    if [ -z ${BUILD_SCRIPTS} ]; then
        echo BUILD_SCRIPTS parameter was not found in buildVars.sh.  Please refer to https://bitbucket.org/nrg/build_scripts buildVars.sh.template for information.
        exit 1
    fi

    if [ -z ${BUILD_SCRIPTS_OWN} ]; then
        echo BUILD_SCRIPTS_OWN parameter was not found in buildVars.sh.  Please refer to https://bitbucket.org/nrg/build_scripts buildVars.sh.template for information.
        exit 1
    fi

    if [ -z ${BUILD_SCRIPTS_REV} ]; then
        BUILD_SCRIPTS_REV=tip
        echo tip will be used as commit version for ${BUILD_SCRIPTS}
    fi

    echo "Cloning build scripts repo"
    echo "hg clone -r ${BUILD_SCRIPTS_REV} ssh://hg@${site}/${BUILD_SCRIPTS_OWN}/${BUILD_SCRIPTS}"
    hg clone -r ${BUILD_SCRIPTS_REV} ssh://hg@${site}/${BUILD_SCRIPTS_OWN}/${BUILD_SCRIPTS}

fi

cd ${homedir}/${project,,}_build/${CUR_HOST}
cp -rf ${BUILD_SCRIPTS}/* .

# Create postgres login file
./buildPGPass.sh

# If vm owner was supplied, make this user an admin on the XNAT deploy
if [ ! -z ${xnatid} ]; then
    ./mkAdmin.sh ${xnatid}
fi

