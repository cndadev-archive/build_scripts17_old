#!/bin/bash

# Bit cludgy, but works for now.   
# New directory, newJars, containing signed jars
# will be created inside execution directory.
# Once script has completed, copy contents of newJars into current
# applet directory to overwrite existing jars and pack.gz files.
# Before running this, you will need to:
# 1.  Set $CERT and $NRGPASS
# 2.  Create a manifest.tmp file in same directory containing Permissions,
#     Codebase, and Application attributes.
# 3.  Create $INPUT_FILE with list of jars you wish to run. If
#     something happens to interrupt execution, you can update 
#     file, so you don't have to start from the beginning.


SCRIPTNAME=processAppletJars.sh
INPUT_FILE=$1

# You must also export variables CERT and NRGPASS at the command line before script is run
# To generate an input file in the applet directory:  ls | grep .jar | grep -v .pack.gz > jarList.txt

export PATH=$PATH:/usr/lib/jvm/java-1.7.0-oracle.x86_64/bin

if [ -d ./newJars ]; then
    rm -rf ./newJars
fi
mkdir ./newJars

while read h 
do
    echo $h
    mkdir ./tmp_${h} 
    cp $h ./tmp_${h}
    cd ./tmp_${h}	
    echo "Exploding ${h}"
    echo We are here:
    pwd
    jar -xvf ${h}
    rm ${h}
    echo "Removing old certificates for ${h}"
    rm ./META-INF/*.SF
    rm ./META-INF/*.RSA
    rm ./META-INF/*.DSA
    echo "Jarring ${h}"
    jar cfm ../newJars/${h} ../manifest.tmp *
    cd ../newJars
    echo "Signing ${h}"
    pack200 --repack ${h}
    jarsigner -keystore ${CERT} -storepass ${NRGPASS} -keypass ${NRGPASS} -tsa http://timestamp.digicert.com ${h} NRG
    echo Verifying prepacked jar
    jarsigner -verify ./${h}
    echo "Packing ${h}"
    pack200 ${h}.pack.gz ${h}
    echo We are here:
    pwd
    mkdir ./tmp_${h}
    cp ${h}.pack.gz ./tmp_${h}
    cd ./tmp_${h}
    unpack200 ${h}.pack.gz ${h}
    echo Veriyfing postpacked jar
    jarsigner -verify ./${h}
    cd ..
    rm -rf tmp_${h}
    cd ..
    rm -rf tmp_${h}
done < ${INPUT_FILE}
