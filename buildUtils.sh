#!/bin/bash

# This function takes one parameter and downloads the zip of the provided repository tip
function downloadBBRepo {
   startDir=`pwd`
   echo entering downloadBBRepo
   buildPath=$1
   curRepo=$2;
   curRepoOwn=$3;
   curRepoRev=tip;
   if [ ! -z $4 ]; then
       curRepoRev=$4
   fi
   echo Downloading ${curRepoOwn}/${curRepo}/${curRepoRev}
   if [ ! -z ${BU} ] && [ ! -z ${BP} ]; then
       echo "curl -u user:pwd ${HTTPS_BITBUCKET}/${curRepoOwn}/${curRepo}/get/${curRepoRev}.zip > ${buildPath}/${curRepo}.zip"
       curl -u ${BU}:${BP} ${HTTPS_BITBUCKET}/${curRepoOwn}/${curRepo}/get/${curRepoRev}.zip > ${buildPath}/${curRepo}.zip
   else
       echo "curl ${HTTPS_BITBUCKET}/${curRepoOwn}/${curRepo}/get/${curRepoRev}.zip > ${buildPath}/${curRepo}.zip"
       curl ${HTTPS_BITBUCKET}/${curRepoOwn}/${curRepo}/get/${curRepoRev}.zip > ${buildPath}/${curRepo}.zip
   fi
   if [ ! -s ${buildPath}/${curRepo}.zip ]; then
     echo Cannot find ${buildPath}/${curRepo}.zip
     exit 1;
   fi
   cd ${buildPath}
   unzip -d ${curRepo}_unzip ${curRepo}.zip
   if [ -d ${buildPath}/${curRepo}_unzip ]; then
      cd ${buildPath}/${curRepo}_unzip
      mv * ${buildPath}/${curRepo}
      cd ${buildPath}
      rm -rf ${buildPath}/${curRepo}_unzip
      rm ${buildPath}/${curRepo}.zip
   else
      echo No ${buildPath}/${curRepo}_unzip exists
      exit 1
   fi
   cd ${startDir}
   echo exiting downloadBBRepo
}

function downloadGHRepo {
   startDir=`pwd`
   echo entering downloadGHRepo
   buildPath=$1
   curRepo=$2;
   curRepoOwn=$3;
   curRepoRev=master;
   if [ ! -z $4 ]; then
       curRepoRev=$4
   fi
   echo Downloading ${curRepoOwn}/${curRepo}/${curRepoRev}
   if [ ! -z ${GU} ] && [ ! -z ${GP} ]; then
       echo "curl -u user:pwd -o ${buildPath}/${curRepo}.zip ${HTTPS_GITHUB}/${curRepoOwn}/${curRepo}/archive/${curRepoRev}.zip"
       curl -u ${GU}:${GP} -o ${buildPath}/${curRepo}.zip ${HTTPS_GITHUB}/${curRepoOwn}/${curRepo}/archive/${curRepoRev}.zip
   else
       echo "curl -o ${buildPath}/${curRepo}.zip ${HTTPS_GITHUB}/${curRepoOwn}/${curRepo}/archive/${curRepoRev}.zip"
       curl -o ${buildPath}/${curRepo}.zip ${HTTPS_GITHUB}/${curRepoOwn}/${curRepo}/archive/${curRepoRev}.zip
   fi
   if [ ! -s ${buildPath}/${curRepo}.zip ]; then
     echo Cannot find ${buildPath}/${curRepo}.zip
     exit 1;
   fi
   cd ${buildPath}
   unzip -d ${curRepo}_unzip ${curRepo}.zip
   if [ -d ${buildPath}/${curRepo}_unzip ]; then
      cd ${buildPath}/${curRepo}_unzip
      mv * ${buildPath}/${curRepo}
      cd ${buildPath}
      rm -rf ${buildPath}/${curRepo}_unzip
      rm ${buildPath}/${curRepo}.zip
   else
      echo No ${buildPath}/${curRepo}_unzip exists
      exit 1
   fi
   cd ${startDir}
   echo exiting downloadGHRepo
}



# If an error occurs, this method outputs the error and ends the script
die(){
  echo >&2 "$@"
  exit 1
}

export -f downloadBBRepo
export -f die

echo "Successfully sourced buildUtils.sh"
